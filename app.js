var express = require('express');
var app = express();
var http = require('http').createServer(app);

var io = require('socket.io')(http);

var fs = require('fs');

var messages = [];
var users = [];

var storeMessage = function (nickname, message, time) {
    messages.push({'message': message, 'nickname': nickname, 'time': time});
    if (messages.length > 20) {
        messages.shift();
    }
}

io.on('connection', function (server) {

    // listen on "join" event and fire the event "add chatter" to all other connected clients
    server.on('join', function (nickname) {
        server.nickname = nickname;

        console.log('on join 1', server.nickname, users);

        users.push(server.nickname);

        console.log('on join 2', server.nickname, users);

        server.broadcast.emit('add chatter', users);
        server.emit('add chatter', users);

        if (messages.length > 1) {
            server.emit('history', messages);
        }
    });

    // listen on "message" event and fire the event "message" to all other connected clients
    server.on('message', function (message, time) {
        var nickname = server.nickname;
        server.broadcast.emit('message', {'message': message, 'nickname': nickname, 'time': time});
        storeMessage(nickname, message, time);
    });

    // listen on "disconnect" event and fire the event "user disconnected" to all other connected clients
    server.on('disconnect', function () {
        console.log('disconnect', server.nickname);
        server.broadcast.emit('user disconnected', server.nickname);
        var index = users.indexOf(server.nickname);
        users.splice(index, 1);
    });

});

http.listen(9000);