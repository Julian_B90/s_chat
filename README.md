Interner Chat
=============

Erläuterung zu der Dateistruktur:
-----------------------------------
    Server:
        app.js

    Client:
        index.html
            /node_modules/jquery/dist/jquery.min.js
            /client/socket.io/1.3.5/socket.io.js
            /node_modules/moment/min/moment.min.js
            /node_modules/angular/angular.min.js
            /node_modules/angular-route/angular-route.min.js
            /node_modules/angular-cookies/angular-cookies.min.js
            /client/custom/js/client.js (Hier ist die Hauptabhandlung des Client enthalten)
            /app-services/userList.service.js
            /app-services/flash.service.js
            /node_modules/bootstrap/dist/css/bootstrap.min.css
            /node_modules/bootstrap/dist/css/bootstrap-theme.min.css
            /client/custom/css/stlye.css
       
    Views:
        login.html
        chat.html


Installation des Servers:
---------------

Installieren von Node.js
https://nodejs.org/download/

Socket.io
~~~
npm install socket.io
~~~

Express
~~~
npm install express
~~~

Forever
~~~
npm install forever -g
~~~