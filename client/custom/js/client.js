
(function(window, angular, undefined) {

    var app = angular.module('client', ['ngRoute', 'ngCookies']);
    var socket = io.connect('http://localhost:9000');

    app.config(['$routeProvider',
        function($routeProvider) {
            /**
             * $routeProvider
             */
            $routeProvider
                .when('/chat', {
                    controller: 'ClientCtrl',
                    templateUrl: 'chat.html'
                })
                .when('/login', {
                    controller: 'LoginCtrl',
                    templateUrl: 'login.html'
                })
                .when('/', {
                    controller: 'LoginCtrl',
                    templateUrl: 'login.html'
                })
                .otherwise({
                    redirectTo: '/login'
                });
        }
    ])

    app.run(['$rootScope', '$cookies', '$location', function($rootScope, $cookies, $location) {

        $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl) {
                console.log('locationChange', newUrl, oldUrl);
                var nickname = $cookies.get('nickname');

                if(newUrl.indexOf('#/chat') != -1) {
                    $rootScope.nickname = nickname;
                    socket.emit('join', $rootScope.nickname);
                }

                var auth = $cookies.get('auth');
                if (!auth) {
                    $location.path('/');
                }
        });

    }]);

    app.controller('LoginCtrl', ['$scope', '$location', '$rootScope', '$cookies', 'FlashService', function($scope, $location, $rootScope, $cookies, FlashService) {

        $scope.login = login;
        var allowedUser = {
            'julian': 'test',
            'tim': 'test',
            'klaus': 'test'
        };

        function login() {
            $scope.dataLoading = true;
            if (allowedUser[$scope.nickname] != undefined && allowedUser[$scope.nickname] == $scope.password) {
                $rootScope.nickname = $scope.nickname;
                $cookies.put('auth', true);
                $cookies.put('nickname', $scope.nickname);
                $location.path('/chat');
            } else {
                FlashService.Error('Username or password wrong.');
                $scope.dataLoading = false;
            }
        };

    }]);

    app.controller('ClientCtrl', ['$scope', '$location', '$rootScope', '$cookies', function($scope, $location, $rootScope, $cookies) {

        $scope.users = [];
        $scope.messages = [];

        socket.on('connect', function(data) {

            if ($rootScope.nickname == undefined) {
                $location.path('/');
            }

            $scope.users.push($rootScope.nickname);
            $scope.nickname = $rootScope.nickname;
            socket.emit('join', $rootScope.nickname);

            if($location.url() != '/chat') {
                $location.path('/chat');
            }

        });

        socket.on('add chatter', function(users) {
            console.log('on add chatter 1', $scope.users, users);
            $scope.users = users;
            console.log('on add chatter 2', $scope.users);
            $scope.$digest();
        });

        socket.on('message', function(message) {
            $scope.messages.push(message)
            // scroll to last message
            scrollToBottom();
            $scope.$digest();
        });

        socket.on('history', function(messages) {
            $scope.messages = messages;
            $scope.$digest();
        });

        socket.on('user disconnected', function(nickname) {
            var index = $scope.users.indexOf(nickname);
            $scope.users.splice(index, 1);
            $scope.$digest();
        });

        jQuery(function($) {
            $('#sendMessage').on('click', function(ev) {
                ev.preventDefault();
                var messageInput = $('#message_input');
                var message = messageInput.val();
                messageInput.val('');
                var currentDate = moment().format('DD.MM.YYYY HH:mm');
                $scope.messages.push({
                    'message': message,
                    'nickname': $rootScope.nickname,
                    'time': currentDate
                });
                socket.emit('message', message, currentDate);
                scrollToBottom();
                $scope.$digest();
            });
        });
    }]);

    function scrollToBottom() {
        $("#chatMessages").animate({
            scrollTop: $("#chatMessages")[0].scrollHeight
        }, 300);
    }
})(window, window.angular);